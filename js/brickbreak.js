var game = new Phaser.Game(800, 600, Phaser.AUTO, 'content', { preload: preload, create: create, update: update });
//entity groups
var balls;
var paddles;
var bricks;

var versionNumber = '0.1.54a';

var score;
var scoreText;
var lives;
var livesText;
var currentLevel;
var currentLevelText;
var gameOverText;
var ballOnPaddle;
 
function preload() {
    game.load.image('paddle', 'assets/paddle.png');
    game.load.image('ball', 'assets/ball.png');
    game.load.image('oneBrick', 'assets/singlehit.png');
    game.load.image('twoBrick', 'assets/twohit.png');
    game.load.image('background', 'assets/background.png');
    console.log('Isolated\'s Brick Breaker - Current Version: ' + versionNumber);
}

function create() {
    //generate background and disable bottom boundcheck.
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.physics.arcade.checkCollision.down = false;
    background = game.add.tileSprite(0, 0, 800, 600, 'background');
    game.input.onDown.add(releaseBall, this);
    firstPlay();
    game.input.onDown.add(releaseBall, this);
}
 
function update() {
    var inputX = Math.min( Math.max(0, game.input.x), [game.width - 80]);
    //allows paddle movement with mouse
    paddle.body.x = inputX;

    if (ballOnPaddle)
    {
        ball.x = paddle.x;
    }
    else
    {
        game.physics.arcade.collide(ball, bricks, ballHitBrick, null, this);
        game.physics.arcade.collide(paddle, ball, ballHitPaddle, null, this);
    }

}

function firstPlay() {
    //reset to original state
    resetVariables();
    //create entities
    createPaddle();
    createBall();
    createBricks(currentLevel);
    //createHUD();
}

function resetVariables() {
    score = 0;
    scoreText = 'Score: ' + score;
    ballOnPaddle = true;
    lives = 2;
    livesText = 'Lives: ' + lives;
    currentLevel = 1;
    currentLevelText = 'Level: ' + currentLevel;
}

function createPaddle() {
    paddles = game.add.group();
    paddles.enableBody = true;
    paddles.physicsBodyType = Phaser.Physics.ARCADE;

    paddle = game.add.sprite(game.world.centerX, 500, 'paddle');
    paddle.anchor.setTo(0.5, 0.5);
    game.physics.enable(paddle, Phaser.Physics.ARCADE);
    paddle.body.collideWorldBounds = true;
    paddle.body.bounce.set(1);
    paddle.body.immovable = true;
}

function createBall() {
    balls = game.add.group();
    balls.enableBody = true;
    balls.physicsBodyType = Phaser.Physics.ARCADE;

    ball = game.add.sprite(game.world.centerX, paddle.y - 9, 'ball');
    ball.anchor.set(0.5);
    ball.checkWorldBounds = true;
    game.physics.enable(ball, Phaser.Physics.ARCADE);
    ball.body.collideWorldBounds = true;
    ball.body.bounce.set(1);
    ball.events.onOutOfBounds.add(ballLost, this);
}

function createBricks(_currentLevel) {
    bricks = game.add.group();
    bricks.enableBody = true;
    bricks.physicsBodyType = Phaser.Physics.ARCADE;

    for (var y = 0; y < 4; y++)
    {
        for (var x = 0; x < 12; x++)
        {
            /*if ((x + y) % 2 == 0)
            {
                brick = bricks.create(64 + x * 56, 50 + y * 52, 'oneBrick');
                brick.health = 1;
            }
            else
            {
                brick = bricks.create(64 + x * 56, 50 + y * 52, 'twoBrick');
                brick.health = 2;
            }*/ //removed until levels are ready
            brick = bricks.create(64 + x * 56, 50 + y * 52, 'oneBrick');
            brick.health = 1;
            brick.body.bounce.set(1);
            brick.body.immovable = true;
        }
    }
}
//function createHUD() { //removed until HUD is ready to implement
    //stuff goes here
    //scoreText = game.add.text(32, 550, 'score: 0', { font: "20px Arial", fill: "#ffffff", align: "left" });
    //livesText = game.add.text(680, 550, 'lives: 3', { font: "20px Arial", fill: "#ffffff", align: "left" });
    //gameOverText = game.add.text(game.world.centerX, 400, '- click to start -', { font: "40px Arial", fill: "#ffffff", align: "center" });
    //gameOverText.anchor.setTo(0.5, 0.5);
//}

function ballLost() {
    lives--;
    livesText.text = 'Lives: ' + lives;
    ball.kill();

    console.log('lol you died');

    if (lives == 0)
    {
        gameOver();
    }
    else 
    {
        ballOnPaddle = true;
        ball.reset(paddle.x - 3, paddle.y - 9);
    }
}

function releaseBall() {
    if (ballOnPaddle)
    {
        ballOnPaddle = false;
        ball.body.velocity.y = -200;
        ball.body.velocity.x = -50
    }
}

function gameOver() {
    console.log('you lost');
    killEntities();
}

function killEntities() {
    
    paddle.kill();
    //paddles.removeAll(); //commented out for single paddle implementation
    bricks.removeAll();
    ball.kill();
    //balls.removeAll(); //commented out for single ball
}

//function levelComplete(_currentLevel) { //removed until levels implemented

//}

function ballHitPaddle(paddle, ball) {
    var diff = 0;

    if (ball.x < paddle.x)
    {
        diff = paddle.x - ball.x;
        ball.body.velocity.x = (-5 * diff);
    }
    else if (ball.x > paddle.x)
    {
        diff = ball.x -paddle.x;
        ball.body.velocity.x = (5 * diff);
    }
}

function ballHitBrick(ball, brick) {
    console.log('lolwut');
    brick.kill();

    if (bricks.countLiving() == 0)
    {
        gameOver();
    }
}
* Simple brick break remake
* 5 levels 
* 1. all single hit bricks
* 2. all two-hit hit bricks
* 3. moving single and two-hit bricks 
* 4. stationary boss - 3 pieces - 2 hits on gun - 3 hits on shield generator - 1 hit on hull - fires lasers in random directions
* 5. moving boss (?) - 3 pieces - 5 hits on gun - 5 hits on shield generator - 4 on hull - fires missiles at paddle